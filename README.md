# tessdata_heb_rashi

[Tesseract OCR engine](https://github.com/tesseract-ocr/tesseract) trained models ([LSTM](https://en.wikipedia.org/wiki/Long_short-term_memory)) for the optical recognition of the Hebrew [Rashi script](https://en.wikipedia.org/wiki/Rashi_script).

This repository contains both - the best and the fast models as well as [instructions](tesseract_4.1.1/TRAINING.md) for their generation along with some [initial input data](tesseract_4.1.1/langdata). The [best model](tesseract_4.1.1/heb_rashi.traineddata) provides higher accuracy but is slower. The [fast model](tesseract_4.1.1/heb_rashi_fast.traineddata) is less accurate but significantly faster. It is recommended to use a [specialized Hebrew spelling dictionary](https://gitlab.com/pninim.org/he_TORANIT) for digitizing Torah related literature.


# Usage

Download either the best ([heb_rashi.traineddata](tesseract_4.1.1/heb_rashi.traineddata)) or the fast model ([heb_rashi_fast.traineddata](tesseract_4.1.1/heb_rashi_fast.traineddata)), depending on your needs, into `tessdata` directory, and supply its path along with other parameters to the Tesseract engine:

## Command line

In order to recognize the file `Brakhot_2a.jpg` into `Brakhot_2a.txt` using the fast model run:

```
tesseract --tessdata-dir /usr/share/tesseract-ocr/4.00/tessdata/ Brakhot_2a.jpg Brakhot_2a -l heb_rashi_fast
```

## Graphical user interface

[gImageReader](https://github.com/manisandro/gImageReader) is a cross-platform GUI for Tesseract. The `tessdata` directory is listed at `Settings -> Preferences -> Language definitions path`.


# Versioning

Current version of `tessdata_heb_rashi` is `4.1`, which means release `1` suitable for the Tesseract version `4`.


# Copyright

[Apache-2.0 License](LICENSE)

Copyright 2021 Zeev Pekar <https://gitlab.com/pninim.org/tessdata_heb_rashi>
